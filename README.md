# temperature-alert


## Environment

- Target framework: .net6.0 (C#)

- Target runtime: win-x86

## Getting started

This is a self-contained console application that can be run in windows stand-alone. 

Download and double-click the **temperature-alert.exe** file in: [Google Drive](https://drive.google.com/file/d/1ZnPopqg-YmDZtGyoV6HGw-EvRk3wHOYy/view?usp=sharing) (You may receive warning from Google, please accept it.)

We assume that input freezing point, boiling point and fluctuation are valid decimal, and the input for temperatures should be seperated with one space. You should able to get the console as below:
```
Please provide the freezing point:0
Please provide the boiling point:100
Please provide the fluctuation:0.5
Freezing point: 0   Boiling point: 100   Fluctuation: +/-0.5
Enter the temperature(s), seperated by one space, for example 0 0.5 1 :4.0 1.5 1.0 0.5 0.0 -0.5 0.5 0.0 -1.0 -3.0 1.0 2.0 5.0
4.0 1.5 1.0 0.5 0.0 freezing -0.5 0.5 0.0 -1.0 -3.0 1.0 unfreezing 2.0 5.0
Enter the temperature(s), seperated by one space, for example 0 0.5 1 :0 -1 0.5 1 2
0 freezing -1 0.5 1 unfreezing 2
Enter the temperature(s), seperated by one space, for example 0 0.5 1 :99 99.5 100 100.5 99.5 99
99 99.5 100 boiling 100.5 99.5 99 unboiling
```

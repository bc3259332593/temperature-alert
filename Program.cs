﻿class Program
{
    static void Main(string[] args)
    {
        Console.Write("Please provide the freezing point:");
        string sFreezePoint = Console.ReadLine();
        Console.Write("Please provide the boiling point:");
        string sBoilingPoint = Console.ReadLine();
        Console.Write("Please provide the fluctuation:");
        string sFluctuation = Console.ReadLine();
        Console.WriteLine("Freezing point: {0}   Boiling point: {1}   Fluctuation: +/-{2}", sFreezePoint, sBoilingPoint, sFluctuation);
        while (true) {
            Console.Write("Enter the temperature(s), seperated by one space, for example 0 0.5 1 :");
            string sTemperature = Console.ReadLine();
            Console.WriteLine(AlertTemperature(sTemperature, decimal.Parse(sFreezePoint), decimal.Parse(sBoilingPoint), decimal.Parse(sFluctuation)));
        }

    }

    static string AlertTemperature(String sTemperature, decimal dFreezePoint, decimal dBoilingPoint, decimal dFluctuation) {
        string[] arrTemp = sTemperature.Split(' ');
        bool bIsFreezing = false;
        bool bIsBoiling = false;
        List<string> list = new List<string>();
        foreach (string sTemp in arrTemp) {
            decimal dTemp;
            if (decimal.TryParse(sTemp, out dTemp)) {
                list.Add(sTemp);
                if (dTemp <= dFreezePoint)
                {
                    if (!bIsFreezing) {
                        list.Add("freezing");
                        bIsFreezing = true;
                        bIsBoiling = false;
                    }
                }
                else if (dTemp > dFreezePoint && dTemp < dBoilingPoint)
                {
                    if (bIsFreezing && dTemp > (dFreezePoint + dFluctuation))
                    {
                        list.Add("unfreezing");
                        bIsFreezing = false;
                    }
                    if (bIsBoiling && dTemp < (dBoilingPoint - dFluctuation))
                    {
                        list.Add("unboiling");
                        bIsBoiling = false;
                    }

                }
                else if (dTemp >= dBoilingPoint)
                {
                    if (!bIsBoiling) {
                        list.Add("boiling");
                        bIsBoiling = true;
                        bIsFreezing = false;
                    }
                }
            }
        }

        return String.Join(" ", list.ToArray());
    }
}